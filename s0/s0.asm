;
; Copyright (c) 2018, Marc Thrun
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
; 	* Redistributions of source code must retain the above copyright
; 	  notice, this list of conditions and the following disclaimer.
; 	* Redistributions in binary form must reproduce the above copyright
; 	  notice, this list of conditions and the following disclaimer in the
; 	  documentation and/or other materials provided with the distribution.
; 	* Neither the name of the <organization> nor the
; 	  names of its contributors may be used to endorse or promote products
; 	  derived from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;


bits 16
org 0x7c00

_start:
    ; Init segment registers ans sp
    mov ax, 0x7c00
    mov sp, ax
    xor ax, ax
    mov ss, ax
    mov ds, ax
    mov ax, 0x1000
    mov es, ax

    ; Keep drive number
    mov [drive_num], dl

    call test_edd

    mov dl, [drive_num]
    call load_sectors

    ; Jump to next stage
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov dl, [drive_num]
    jmp 0x0000:0x8000

; Test for EDD availability and return if present
; dl: drive number
;
; clobbered: ax, bx, dh, cx
test_edd:
    ; Test for EDD
    xor dh, dh
    mov ax, 0x4100
    mov bx, 0x55aa
    int 0x13
    jc .error_edd
    cmp bx, 0xaa55
    jne .error_edd
    and cx, 0x1+0x4
    xor cx, 0x1+0x4
    jnz .error_edd
    ret
.error_edd:
    mov si, error_msg_edd
    jmp error

; Load all sectors to 0x1000:0000
; dl: drive number
;
; clobbered: ax, cx
load_sectors:
    mov si, dap
    mov ah, 0x42
    int 0x13
    jc .error_read
    ret
.error_read:
    mov si, error_msg_read
    jmp error


; Write error message and halt
; si: addr of text
error:
    mov bx, si
    mov si, error_msg_base
    mov ah, 0x4f
    call write_text
    mov si, bx
    mov ah, 0x4f
    call write_text
    jmp halt

; Write text to console
; si: addr of text
; ah: color of text
;
; clobbered: ax, es, di
write_text:
    push ax
    mov ax, 0xb800
    mov es, ax
    pop ax
    mov di, [con_offset]

.loop:
    lodsb
    and al, al
    jz .exit
    stosw
    jmp .loop

.exit:
    mov [con_offset], di
    ret

; Halt
halt: jmp halt

; Ro-data
error_msg_base: db "MASH ERROR: ", 0
error_msg_edd: db "EDD", 0
error_msg_read: db "READ", 0

; Data
con_offset: dw 0
drive_num: db 0

; Ensure fixed address of DAP so it can be modified easily
times 430-($-$$) db 0x90
dap:
dap_size: db 0x10
db 0
dap_sec_count: dw 128
dap_off: dw 0
dap_seg: dw 0x0800
dap_start: dd 2048
dd 0

; Partition table
times 64 db 0

; End
times 510-($-$$) db 0
dw 0xAA55
