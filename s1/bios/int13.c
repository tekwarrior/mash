/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "int13.h"

RMCODE u8 int13_edd_install_check(u8 drive) {
    u16 ax, bx;
    u32 flags;
    asm volatile(
    "calll pm_to_rm\n"
    ".code16\n"
    "int $0x13\n"
    "calll rm_to_pm\n"
    ".code32\n"
    "pushfl\n"
    "popl %%edx\n"
    : "=a"(ax), "=b"(bx), "=d"(flags)
    : "a"(0x4100), "b"(0x55aa), "d"((u16) drive)
    : "ecx", "edi", "esi", "memory"
    );

    if (flags & EFLAGS_CARRY) return 0;
    if (bx != 0xaa55) return 0;
    return (u8) ((ax >> 8) & 0xff);
}

RMCODE u8 int13_edd_get_disk_parameters(void *buf, u8 drive) {
    u16 ax;
    u32 flags;
    u16 ds, si;
    ptr_to_rmaddr(buf, &ds, &si);

    asm volatile(
            "xchgw %%bx, %%bx\n"

    "calll pm_to_rm\n"
    ".code16\n"
    "movw %%bx, %%ds\n"
    "int $0x13\n"
    "calll rm_to_pm\n"
    ".code32\n"
    "pushfl\n"
    "popl %%edx\n"
    : "=a"(ax), "=d"(flags)
    : "a"(0x4800), "b"(ds), "d"((u16) drive), "S"(si)
    : "ecx", "edi", "ebp", "memory"
    );

    if (flags & EFLAGS_CARRY) return (u8) (ax >> 8);
    return 0;
}

RMCODE u8 int13_edd_extended_read(disk_edd_address_packet_t *dap, u8 drive) {
    u16 ax;
    u32 flags;
    u16 ds, si;
    ptr_to_rmaddr(dap, &ds, &si);

    asm volatile(
    "calll pm_to_rm\n"
    ".code16\n"
    "movw %%bx, %%ds\n"
    "int $0x13\n"
    "calll rm_to_pm\n"
    ".code32\n"
    "pushfl\n"
    "popl %%edx\n"
    : "=a"(ax), "=d"(flags)
    : "a"(0x4200), "b"(ds), "d"((u16) drive), "S"(si)
    : "ecx", "edi", "ebp", "memory"
    );

    if (flags & EFLAGS_CARRY) return (u8) (ax >> 8);
    return 0;
}

RMCODE u8 int13_edd_extended_write(disk_edd_address_packet_t *dap, u8 write_flags, u8 drive) {
    u16 ax;
    u32 flags;
    u16 ds, si;
    ptr_to_rmaddr(dap, &ds, &si);

    asm volatile(
    "calll pm_to_rm\n"
    ".code16\n"
    "movw %%bx, %%ds\n"
    "int $0x13\n"
    "calll rm_to_pm\n"
    ".code32\n"
    "pushfl\n"
    "popl %%edx\n"
    : "=a"(ax), "=d"(flags)
    : "a"(0x4300 | write_flags), "b"(ds), "d"((u16) drive), "S"(si)
    : "ecx", "edi", "ebp", "memory"
    );

    if (flags & EFLAGS_CARRY) return (u8) (ax >> 8);
    return 0;
}