/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "tinylib/tinylib.h"
#include "console/console.h"
#include "disk/disk.h"
#include "disk/disk_edd.h"
#include "disk/gpt.h"

static char *part_name(gpt_entry_t *entry) {
    static char name[37];
    for (int i = 0; i < 36; i++) {
        u16 c = entry->name[i];
        if (i > 0xff) name[i] = '?';
        else if (i > 0) name[i] = (char)c;
        else name[i] = 0;
    }
    return name;
}

void main(void) {
    disk_driver_interface_t *disk_driver = disk_get_driver("disk");
    if (!disk_driver) abort("no disk driver found");
    part_driver_interface_t *part_driver = part_get_driver("gpt");
    if (!part_driver) abort("no part driver found");

    int drive_count = disk_scan(disk_driver);
    con_printf("Detected %d drives\n", drive_count);

    for (int i = 0; i < drive_count; i++) {
        disk_interface_t *disk = disk_open(disk_driver, i);
        disk_edd_disk_interface_t *edd_disk = (disk_edd_disk_interface_t*)disk;

        if (!disk) {
            con_printf("disk %2u: unable to open disk\n", i);
            continue;
        }
        const disk_info_t *info = disk_get_info(disk);
        con_printf("disk %2u: drive %02x edd %02x - %016llx @ %lu bytes\n", i, edd_disk->drive_number, edd_disk->edd_version, info->block_count, info->block_size);

        int part_count = part_scan(part_driver, disk);
        con_printf("detected %d gpt partitions\n", part_count);

        for (int part_index = 0; part_index < part_count; part_index++) {
            part_interface_t *part = part_open(part_driver, disk, part_index);
            gpt_part_interface_t *gpt_part = (gpt_part_interface_t*)part;

            if (!part) {
                con_printf("p%-4d: error opening partition\n", part_index);
                continue;
            }

            gpt_entry_t *part_entry = &gpt_part->entry;
            if (guid_cmp(&part_entry->type_guid, &guid_empty) == 0) continue;
            u64 size = part_entry->last_lba - part_entry->first_lba;
            con_printf("p%-4d: type=%s (%s)\n", part_index, guid_to_str(&part_entry->type_guid, 0), gpt_part_type_name(&part_entry->type_guid));
            con_printf("       guid=%s attr=%016llx\n", guid_to_str(&part_entry->entry_guid, 0), part_entry->attr);
            con_printf("       from=%016llx to=%016llx size=%016llx\n", part_entry->first_lba, part_entry->last_lba, size);
            con_printf("       name=\"%s\"\n", part_name(part_entry));

            part_close(part);
        }
        disk_close(disk);
    }
}

