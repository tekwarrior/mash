/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "interrupt.h"
#include "tinylib/tinylib.h"

enum {
    PIC1_CMD = 0x20,
    PIC1_DATA = PIC1_CMD+1,
    PIC2_CMD = 0xA0,
    PIC2_DATA = PIC2_CMD+1,
};

enum {
    PIC_ICW1_INIT = 0x10,
    PIC_ICW1_HAS_ICW4 = 0x01,
    PIC_ICW4_8086_MODE = 0x01,

    PIC2_SLAVE_ID = 2,
    PIC1_SLAVE_PIN = 1 << PIC2_SLAVE_ID,
};

void interrupt_remap_pic(u8 master_base, u8 slave_base) {
    u8 mask1 = in8(PIC1_DATA);
    u8 mask2 = in8(PIC2_DATA);

    out8(PIC1_CMD, PIC_ICW1_INIT|PIC_ICW1_HAS_ICW4);
    out8(PIC2_CMD, PIC_ICW1_INIT|PIC_ICW1_HAS_ICW4);
    out8(PIC1_DATA, master_base);
    out8(PIC2_DATA, slave_base);
    out8(PIC1_DATA, PIC1_SLAVE_PIN);
    out8(PIC2_DATA, PIC2_SLAVE_ID);
    out8(PIC1_DATA, PIC_ICW4_8086_MODE);
    out8(PIC2_DATA, PIC_ICW4_8086_MODE);

    out8(PIC1_DATA, mask1);
    out8(PIC2_DATA, mask2);
}

void interrupt_remap_pic_pm() {
    interrupt_remap_pic(IRQ_PM_BASE, IRQ_PM_BASE + 8);
}

void interrupt_remap_pic_rm() {
    interrupt_remap_pic(IRQ_RM_MASTER_BASE, IRQ_RM_SLAVE_BASE);
}

#define INT_BIOS_IRQ_DEFINE(number) \
static RMCODE void interrupt_call_bios_ ## number() { \
    asm volatile( \
        "calll pm_to_rm\n" \
        ".code16\n" \
        "int $0x" # number "\n" \
        "calll rm_to_pm\n" \
        ".code32\n" \
    ); \
}

INT_BIOS_IRQ_DEFINE(08)
INT_BIOS_IRQ_DEFINE(09)
INT_BIOS_IRQ_DEFINE(0a)
INT_BIOS_IRQ_DEFINE(0b)
INT_BIOS_IRQ_DEFINE(0c)
INT_BIOS_IRQ_DEFINE(0d)
INT_BIOS_IRQ_DEFINE(0e)
INT_BIOS_IRQ_DEFINE(0f)
INT_BIOS_IRQ_DEFINE(70)
INT_BIOS_IRQ_DEFINE(71)
INT_BIOS_IRQ_DEFINE(72)
INT_BIOS_IRQ_DEFINE(73)
INT_BIOS_IRQ_DEFINE(74)
INT_BIOS_IRQ_DEFINE(75)
INT_BIOS_IRQ_DEFINE(76)
INT_BIOS_IRQ_DEFINE(77)

static void (*int_bios_irq_table[])() = {
        interrupt_call_bios_08,
        interrupt_call_bios_09,
        interrupt_call_bios_0a,
        interrupt_call_bios_0b,
        interrupt_call_bios_0c,
        interrupt_call_bios_0d,
        interrupt_call_bios_0e,
        interrupt_call_bios_0f,
        interrupt_call_bios_70,
        interrupt_call_bios_71,
        interrupt_call_bios_72,
        interrupt_call_bios_73,
        interrupt_call_bios_74,
        interrupt_call_bios_75,
        interrupt_call_bios_76,
        interrupt_call_bios_77,
};

void interrupt_handler(interrupt_state_t *state) {
    if (state->irq < 0x20) {
        con_printf("exception: %02lx %08lx f=%08lx\n", state->irq, state->error, state->eflags);
        con_printf("           eip=%08lx esp=%08lx\n", state->eip, state->unused_esp + 5 * 4);
        abort("CPU EXCEPTION");
    }

    u32 irq = state->irq - IRQ_PM_BASE;
    if (irq < IRQ_COUNT) {
        if (irq > 0) con_printf("irq %02lx\n", irq);
        int_bios_irq_table[irq]();
        return;
    }
    con_printf("interrupt: %02lx\n", state->irq);
    abort("unknown interrupt");
}
