/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "memory.h"
#include "tinylib.h"
#include "intops.h"

enum {
	MEM_MAGIC = 0x48414c54,
	MEM_ALLOCATED = 0x1,
    MEM_ALIGNMENT_BITS = 5,
};

typedef struct alloc_header_t {
	u32 magic;
	u32 flags;
	struct alloc_header_t *prev, *next;
} alloc_header_t;

extern alloc_header_t heap_begin[];
extern u8 heap_end[];

static inline void *mem_alloc_data_ptr(alloc_header_t *header) {
	return ((u8*)header) + sizeof (alloc_header_t);
}

static inline alloc_header_t *mem_alloc_header_ptr(void *p) {
	alloc_header_t *header = (alloc_header_t*)(((u8*)p) - sizeof (alloc_header_t));
	if (header->magic != MEM_MAGIC) abort("mem_alloc_header_ptr: corrupted alloc header");
	return header;
}

static inline u32 mem_alloc_size(alloc_header_t *header) {
	return (u32)((u8*)(header->next)-(u8*)(header) - sizeof(alloc_header_t));
}

int mem_cmp(void *a, void *b, u32 size) {
    u8 *pa = a;
	u8 *pb = b;

	while (size--) {
		if (*pa == *pb) pa++, pb++;
		else return *pa - *pb;
	}
	return 0;
}

void mem_set(void *p, u8 value, u32 size) {
    u8 *wp = p;
    while (size--) *wp++ = value;
}

void mem_copy(void *from, void *to, u32 size) {
    u8 *wfrom = from;
    u8 *wto = to;

    if (wfrom + size < wto || wfrom > wto) {
        while (size--) *wto++ = *wfrom++;
    } else {
        while (size--) wto[size] = wfrom[size];
    }
}

void *mem_alloc(u32 size) {
	alloc_header_t *header = heap_begin;
    if (header->magic != MEM_MAGIC) abort("mem_alloc: corrupted first alloc header");

	while (header->next) {
        if (header->magic != MEM_MAGIC) abort("mem_alloc: corrupted alloc header");

        if ((header->flags & MEM_ALLOCATED) == 0 && mem_alloc_size(header) >= size) {
            header->flags |= MEM_ALLOCATED;
            return mem_alloc_data_ptr(header);
        }

		header = header->next;
	}

    // Allocate new chunk
    u32 new_header_offset = align_u32(size + sizeof(alloc_header_t), MEM_ALIGNMENT_BITS);
    alloc_header_t *new_header = (alloc_header_t*)((u8*)header + new_header_offset);
    if ((u8*)new_header > heap_end) return 0;

    new_header->magic = MEM_MAGIC;
    new_header->flags = 0;
    new_header->prev = header;
    header->next = new_header;
    header->flags |= MEM_ALLOCATED;

    return mem_alloc_data_ptr(header);
}

void mem_free(void *p) {
	alloc_header_t *header = mem_alloc_header_ptr(p);
	if (!p) abort("trying to free invalid pointer");

    header->flags &= ~MEM_ALLOCATED;
}

void mem_init() {
	heap_begin->magic = MEM_MAGIC;
	heap_begin->flags = 0;
	heap_begin->prev = 0;
	heap_begin->next = 0;
}