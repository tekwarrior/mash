/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef MEMORY_H
#define MEMORY_H

#include "types.h"

static inline void ptr_to_rmaddr(void *p, u16 *seg, u16 *off) {
    u32 ptr = (u32)p;
    *off = (u16)(ptr & 0xf);
    *seg = (u16)((ptr & 0xffff0) >> 4);
}

static inline void *rmaddr_to_ptr(u16 seg, u16 off) {
    return (void*)(((u32)seg) << 4 | off);
}

#define STATIC_INIT(p) __attribute__((section(".static_init"))) void * __init_ ## p = p;

void mem_init();

int mem_cmp(void *a, void *b, u32 size);

void mem_set(void *p, u8 value, u32 size);
static inline void mem_clr(void *p, u32 size) { mem_set(p, 0, size); }

void mem_copy(void *from, void *to, u32 size);

void *mem_alloc(u32 size);
void mem_free(void *p);

#endif //MEMORY_H

