/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "builtinfn.h"

/*
 * The compiler (clang or gcc) will insert calls to these support functions. To avoid pulling in the whole libgcc
 * or compiler-rt we provide our own implementation for these handful of functions.
 */

u64 __udivdi3(u64 num, u64 den) {
    return __udivmoddi4(num, den, 0);
}

u64 __umoddi3(u64 num, u64 den) {
    u64 rem;
    __udivmoddi4(num, den, &rem);
    return rem;
}

u64 __udivmoddi4 (u64 num, u64 den, u64 *rem) {
    if (den == 0) return 1/(unsigned)den;

    u64 result = 0;
    int shifted = 0;

    while (den < num && !(den & 0x8000000000000000ull)) {
        den <<= 1;
        shifted++;
    }

    do {
        result <<= 1;
        if (den <= num) {
            result |= 1;
            num -= den;
        }

        den >>=1;
    } while (shifted--);

    if (rem) *rem = num;
    return result;
}

s64 __divdi3(s64 num, s64 den) {
    return __divmoddi4(num, den, 0);
}

s64 __moddi3(s64 num, s64 den) {
    s64 rem;
    __divmoddi4(num, den, &rem);
    return rem;
}

s64 __divmoddi4 (s64 num, s64 den, s64 *rem) {
    unsigned minus = 0;

    if (num < 0) {
        num = -num;
        minus++;
    }

    if (den < 0) {
        den = -den;
        minus++;
    }

    u64 urem = 0;
    u64 result = __udivmoddi4((u64)num, (u64)den, &urem);

    if (rem) {
        if (minus & 1) *rem = -(s64)urem;
        else *rem = (s64)urem;
    }

    if (minus & 1) return -(s64)result;
    else return (s64)result;
}
