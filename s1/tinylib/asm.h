/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef ASM_H
#define ASM_H

#include "types.h"

enum {
    EFLAGS_CARRY = 1<<0,
    EFLAGS_PARITY = 1<<2,
    EFLAGS_AUX = 1<<4,
    EFLAGS_ZERO = 1<<6,
    EFLAGS_SIGN = 1<<7,
    EFLAGS_TRAP = 1<<8,
    EFLAGS_INTERRUPT = 1<<9,
    EFLAGS_DIRECTION = 1<<10,
    EFLAGS_OVERFLOW = 1<<11,
    EFLAGS_NESTED = 1<<14,
    EFLAGS_RESUME = 1<<16,
    EFLAGS_V8086 = 1<<17,
    EFLAGS_ALIGNCHK = 1<<18,
    EFLAGS_VINT = 1<<19,
    EFLAGS_VINTPEND = 1<<20,
    EFLAGS_ID = 1<<21,
    EFLAGS_IOPL_SHIFT = 12,
    EFLAGS_IOPL = 3 << EFLAGS_IOPL_SHIFT
};

static inline void out8(u16 port, u8 value) {
    asm volatile("outb %0, %1" :  : "a"(value), "Nd"(port));
}

static inline void out16(u16 port, u16 value) {
    asm volatile("outw %0, %1" :  : "a"(value), "Nd"(port));
}

static inline void out32(u16 port, u32 value) {
    asm volatile("outl %0, %1" :  : "a"(value), "Nd"(port));
}


static inline u8 in8(u16 port) {
    u8 value;
    asm volatile("inb %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}

static inline u16 in6(u16 port) {
    u16 value;
    asm volatile("inw %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}

static inline u32 in32(u16 port) {
    u32 value;
    asm volatile("inl %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}


static inline u16 get_flags() {
    u16 value;
    asm volatile(
        "pushf\n"
        "pop %0"
    : "=g"(value));
    return value;
}

static inline void wrmsr(u32 reg, u64 value) {
    asm volatile ("wrmsr" : : "c" (reg), "A" (value));
}

static inline u64 rdmsr(u32 reg) {
    u64 value;
    asm volatile ("rdmsr" : "=A" (value) : "c" (reg));
    return value;
}

static inline u32 get_cr0() {
    u32 value;
    asm volatile ("movl %%cr0, %0" : "=r"(value) : : "memory");
    return value;
}

static inline u32 get_cr2() {
    u32 value;
    asm volatile ("movl %%cr2, %0" : "=r"(value) : : "memory");
    return value;
}

static inline u32 get_cr3() {
    u32 value;
    asm volatile ("movl %%cr3, %0" : "=r"(value) : : "memory");
    return value;
}

static inline u32 get_cr4() {
    u32 value;
    asm volatile ("movl %%cr4, %0" : "=r"(value) : : "memory");
    return value;
}

static inline void set_cr0(u32 value) {
    asm volatile ("movl %0, %%cr0" : : "Nd"(value) : "memory");
}

static inline void set_cr2(u32 value) {
    asm volatile ("movl %0, %%cr2" : : "Nd"(value) : "memory");
}

static inline void set_cr3(u32 value) {
    asm volatile ("movl %0, %%cr3" : : "Nd"(value) : "memory");
}

static inline void set_cr4(u32 value) {
    asm volatile ("movl %0, %%cr4" : : "Nd"(value) : "memory");
}

static inline int int_enabled() {
    return (get_flags() & (1 << 9)) != 0;
}

static inline void cli() {
    asm volatile("cli");
}

static inline void sti() {
    asm volatile("cli");
}

static inline int int_set_enabled(int enabled) {
    int was_enabled = int_enabled();
    if (enabled) sti();
    else cli();
    return was_enabled;
}

static inline void hlt() {
    asm volatile ("hlt": : :"memory");
}

#endif //ASM_H
