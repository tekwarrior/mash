/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "../console/console.h"


#include "format.h"

#include "types.h"
#include "memory.h"
#include "string.h"

/*
 * Enums for *printf
 */
enum {
    SIZE_CHAR = 0,
    SIZE_SHORT = 1,
    SIZE_INT = 2,
    SIZE_LONG = 3,
    SIZE_LONGLONG = 4,
    SIZE_PTR = SIZE_LONG,
    SIZE_SIZE = SIZE_LONG
};

enum {
    ALIGN_RIGHT = 0,
    ALIGN_LEFT = 1
};

enum {
    CASE_LOWER = 0,
    CASE_UPPER = 1
};

enum {
    INFORMAT_NONE = 0,
    INFORMAT_BEGIN = 1,
    INFORMAT_FIELDLENGTH = 2,
    INFORMAT_SIZE = 3,
};

int fncprintf(void (*emit)(void*,char), void *data, char const *fmt, va_list args) {
    int in_format = INFORMAT_NONE;
    int size = SIZE_INT;
    int zero_fill = 0;
    unsigned field_length = 0;
    int align = ALIGN_RIGHT;
    char num_conv_buf[33];
    const char *next_string_to_write = 0;

    while (*fmt) {
        if (in_format != INFORMAT_NONE) {
            switch (*fmt) {
                // Flags and width
                case '-':
                    if (in_format == INFORMAT_BEGIN) {
                        align = ALIGN_LEFT;
                        in_format = INFORMAT_FIELDLENGTH;
                    }
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    field_length = field_length * 10 + (*fmt - '0');
                    in_format = INFORMAT_FIELDLENGTH;
                    break;
                case '0':
                    if (in_format == INFORMAT_BEGIN) {
                        zero_fill = 1;
                        in_format = INFORMAT_FIELDLENGTH;
                    } else {
                        field_length = field_length * 10;
                    }
                    break;
                    // Type size modifiers
                case 'l':
                    if (size < SIZE_LONGLONG)
                        size++;
                    in_format = INFORMAT_SIZE;
                    break;
                case 'h':
                    if (size > SIZE_CHAR)
                        size--;
                    in_format = INFORMAT_SIZE;
                    break;
                case 'z':
                    size = SIZE_SIZE;
                    in_format = INFORMAT_SIZE;
                    break;
                    // Format types
                case 'd':
                    switch (size) {
                        case SIZE_CHAR:
                            next_string_to_write = itoa(va_arg(args, int) & 0xff, num_conv_buf, 10);
                            break;
                        case SIZE_SHORT:
                            next_string_to_write = itoa(va_arg(args, int) & 0xffff, num_conv_buf, 10);
                            break;
                        case SIZE_INT:
                            next_string_to_write = itoa(va_arg(args, int), num_conv_buf, 10);
                            break;
                        case SIZE_LONG:
                            next_string_to_write = ltoa(va_arg(args, long), num_conv_buf, 10);
                            break;
                        case SIZE_LONGLONG:
                            next_string_to_write = lltoa(va_arg(args, long long), num_conv_buf, 10);
                            break;
                        default:
                            return 0;
                    }
                    in_format = INFORMAT_NONE;
                    break;
                case 'u':
                    switch (size) {
                        case SIZE_CHAR:
                            next_string_to_write = uitoa(va_arg(args, unsigned int) & 0xff, num_conv_buf, 10);
                            break;
                        case SIZE_SHORT:
                            next_string_to_write = uitoa(va_arg(args, unsigned int) & 0xffff, num_conv_buf, 10);
                            break;
                        case SIZE_INT:
                            next_string_to_write = uitoa(va_arg(args, unsigned int), num_conv_buf, 10);
                            break;
                        case SIZE_LONG:
                            next_string_to_write = ultoa(va_arg(args, unsigned long), num_conv_buf, 10);
                            break;
                        case SIZE_LONGLONG:
                            next_string_to_write = ulltoa(va_arg(args, unsigned long long), num_conv_buf, 10);
                            break;
                        default:
                            return 0;
                    }
                    in_format = INFORMAT_NONE;
                    break;
                case 'x':
                    switch (size) {
                        case SIZE_CHAR:
                            next_string_to_write = uitoa(va_arg(args, unsigned int) & 0xff, num_conv_buf, 16);
                            break;
                        case SIZE_SHORT:
                            next_string_to_write = uitoa(va_arg(args, unsigned int) & 0xffff, num_conv_buf, 16);
                            break;
                        case SIZE_INT:
                            next_string_to_write = uitoa(va_arg(args, unsigned int), num_conv_buf, 16);
                            break;
                        case SIZE_LONG:
                            next_string_to_write = ultoa(va_arg(args, unsigned long), num_conv_buf, 16);
                            break;
                        case SIZE_LONGLONG:
                            next_string_to_write = ulltoa(va_arg(args, unsigned long long), num_conv_buf, 16);
                            break;
                        default:
                            return 0;
                    }
                    in_format = INFORMAT_NONE;
                    break;
                case 'X':
                    switch (size) {
                        case SIZE_CHAR:
                            next_string_to_write = uitoa(va_arg(args, unsigned int) & 0xff, num_conv_buf, 16);
                            break;
                        case SIZE_SHORT:
                            next_string_to_write = uitoa(va_arg(args, unsigned int) & 0xffff, num_conv_buf, 16);
                            break;
                        case SIZE_INT:
                            next_string_to_write = uitoa(va_arg(args, unsigned int), num_conv_buf, 16);
                            break;
                        case SIZE_LONG:
                            next_string_to_write = ultoa(va_arg(args, unsigned long), num_conv_buf, 16);
                            break;
                        case SIZE_LONGLONG:
                            next_string_to_write = ulltoa(va_arg(args, unsigned long long), num_conv_buf, 16);
                            break;
                        default:
                            return 0;
                    }
                    // TODO upper case
                    in_format = INFORMAT_NONE;
                    break;
                case 'p':
                    size = SIZE_PTR;
                    next_string_to_write = ultoa((u32)va_arg(args, void*), num_conv_buf, 16);
                    in_format = INFORMAT_NONE;
                    break;
                case 'P':
                    size = SIZE_PTR;
                    next_string_to_write = ultoa((u32)va_arg(args, void*), num_conv_buf, 16);
                    in_format = INFORMAT_NONE;
                    break;
                case 's':
                    next_string_to_write = va_arg(args, const char*);
                    in_format = INFORMAT_NONE;
                    break;
                case 'c':
                    emit(data, (char)va_arg(args, int));
                    in_format = INFORMAT_NONE;
                    break;
                    // Escaped '%'
                case '%':
                    emit(data, '%');
                    in_format = INFORMAT_NONE;
                    break;
                    // TODO floating point support
                default:
                    next_string_to_write = "(unk. format)";
                    in_format = INFORMAT_NONE;
                    break;
            }
        } else {
            switch (*fmt) {
                case '%':
                    in_format = INFORMAT_BEGIN;
                    zero_fill = 0;
                    field_length = 0;
                    size = SIZE_INT;
                    align = ALIGN_RIGHT;
                    break;
                default:
                    emit(data, *fmt);
                    break;
            }
        }

        if (next_string_to_write) {
            u32 string_length = str_len(next_string_to_write);
            u32 fillLength = 0;
            if (string_length < field_length) {
                fillLength = field_length - string_length;
            }
            char fill = zero_fill ? '0' : ' ';
            if (align == ALIGN_RIGHT) {
                for (u32 i = 0; i < fillLength; i++) {
                    emit(data, fill);
                }
            }
            while (*next_string_to_write) {
                emit(data, *next_string_to_write++);
            }
            next_string_to_write = 0;
            if (align == ALIGN_LEFT) {
                for (u32 i = 0; i < fillLength; i++) {
                    emit(data, fill);
                }
            }

        }

        fmt++;
    }
    return 1;
}

struct vsnprintf_data_t {
    char *to;
    u32 max;
    u32 emitted;
    u32 written;
};

static void vsnprintf_emit(void *dataptr, char c) {
    struct vsnprintf_data_t *data = dataptr;

    if (data->to && data->written < data->max - 1) {
        data->to[data->written] = c;
        data->written++;
    }
    data->emitted++;
}

int vsnprintf(char * s, u32 n, const char *fmt, va_list args) {
    struct vsnprintf_data_t data = {
            .to = s,
            .max = n,
            .written = 0,
            .emitted = 0
    };
    int success = fncprintf(vsnprintf_emit, &data, fmt, args);
    if (!success) return -1;

    if (data.to) data.to[data.written] = 0;
    return (int)data.emitted;
}

int vasprintf(char **strp, const char *fmt, va_list args) {
    va_list args_copy;
    va_copy(args_copy, args);
    int len = vsnprintf(0, 0, fmt, args_copy);
    va_end(args_copy);

    if (len < 0) {
        return len;
    }
    u32 buffer_size = (u32)len + 1;
    char *buffer = mem_alloc(buffer_size);
    if (!buffer) return -1;
    *strp = buffer;
    return vsnprintf(buffer, buffer_size, fmt, args);
}


int sprintf(char * s, const char * format, ...) {
    va_list args;
    va_start(args, format);
    int result = vsprintf(s, format, args);
    va_end(args);
    return result;
}

int snprintf(char * s, unsigned n, const char * format, ...) {
    va_list args;
    va_start(args, format);
    int result = vsnprintf(s, n, format, args);
    va_end(args);
    return result;
}

int asprintf(char **strp, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    int result = vasprintf(strp, fmt, args);
    va_end(args);
    return result;
}

int vsprintf(char * s, const char * format, va_list args) {
    return vsnprintf(s, (unsigned)-1, format, args);
}

enum {
    MAX_BASE = 36,
    MIN_BASE = 2
};

static char strtol_digit_table[36] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

char *itoa(int value, char *str, int base) {
    return lltoa(value, str, base);
}

char *ltoa(long value, char *str, int base) {
    return lltoa(value, str, base);
}

char *lltoa(long value, char *str, int base) {
    if (str && value < 0) {
        *str++ = '-';
        value = -value;
    }
    return ulltoa((unsigned long long)value, str, base);
}

char *uitoa(unsigned int value, char *str, int base) {
    return ultoa(value, str, base);
}

char *ultoa(unsigned long value, char *str, int base) {
    return ulltoa(value, str, base);
}

char *ulltoa(unsigned long long value, char *str, int base) {
    char buf[65];
    unsigned index = sizeof buf;
    buf[--index] = 0;

    if (!str) return 0;
    if (base < MIN_BASE) base = MIN_BASE;
    if (base > MAX_BASE) return 0;

    do {
        buf[--index] = strtol_digit_table[value % base];
        value /= base;
    } while (value > 0);

    char *output = str;
    while (index < sizeof buf) {
        *output++ = buf[index++];
    }
    *output = 0;

    return str;
}