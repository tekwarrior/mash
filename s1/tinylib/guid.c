/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#define GUID_CREATE
#include "tinylib.h"

int guid_cmp(guid_t *a, guid_t *b) {
//    con_printf("guid_cmp: %s | %s\n", guid_to_str(a, 0), guid_to_str(b, 0));
//    volatile unsigned c = 0;
//    while (c++ < 100000000);

    if (a->tl > b->tl) return 1;
    else if (a->tl < b->tl) return -1;

    else if (a->tm > b->tm) return 1;
    else if (a->tm < b->tm) return -1;

    else if (a->thv > b->thv) return 1;
    else if (a->thv < b->thv) return -1;

    else if (a->csh > b->csh) return 1;
    else if (a->csh < b->csh) return -1;

    else if (a->csl > b->csl) return 1;
    else if (a->csl < b->csl) return -1;

    else if (a->nh > b->nh) return 1;
    else if (a->nh < b->nh) return -1;

    else if (a->nl > b->nl) return 1;
    else if (a->nl < b->nl) return -1;

    else return 0;
}

char *guid_to_str(guid_t *g, char *buf) {
    static char static_buf[37];
    if (!buf) buf = static_buf;

    sprintf(buf, "%08lx-%04x-%04x-%02x%02x-%08lx%04x", g->tl, g->tm, g->thv, g->csh, g->csl,
               swap_endianness_u32(g->nh), swap_endianness_u16(g->nl));

    return buf;
}