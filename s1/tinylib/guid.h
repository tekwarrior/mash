/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef GUID_H
#define GUID_H

#include "attr.h"
#include "types.h"

typedef struct PACKED guid {
        u32 tl;
        u16 tm;
        u16 thv;
        u8 csh;
        u8 csl;
        u32 nh;
        u16 nl;
} guid_t;

int guid_cmp(guid_t *a, guid_t *b);
static inline int guid_cmpv(guid_t a, guid_t b) { return guid_cmp(&a, &b); }

char *guid_to_str(guid_t *g, char *buf);

#define GUID_VALUE(tl, tm, thv, cs, node) { \
    tl, \
    tm, \
    thv, \
    cs >> 8, \
    cs & 0xff, \
    (((node>>40)&0xff)<<0)|(((node>>32)&0xff)<<8)|(((node>>24)&0xff)<<16)|(((node>>16)&0xff)<<24), \
    (u16)(((node>>8)&0xff)|((node&0xff)<<8)) \
}
#define GUID_DEFINE(n, tl, tm, thv, cs, node) guid_t n = GUID_VALUE(tl, tm, thv, cs, node);

#ifdef GUID_CREATE
#define GUID(n, tl, tm, thv, cs, node) GUID_DEFINE(n, tl, tm, thv, cs, node ## ull)
#else
#define GUID(n, tl, tm, thv, cs, node) extern guid_t n;
#endif

GUID(guid_empty, 0, 0, 0, 0, 0)
GUID(guid_gpt_microsoft_basic, 0xEBD0A0A2, 0xB9E5, 0x4433, 0x87C0, 0x68B6B72699C7)
GUID(guid_gpt_microsoft_reserved, 0xE3C9E316, 0x0B5C, 0x4DB8, 0x817D, 0xF92DF00215AE)
GUID(guid_gpt_microsoft_re, 0xDE94BBA4, 0x06D1, 0x4D40, 0xA16A, 0xBFD50179D6AC)
GUID(guid_gpt_onie_boot, 0x7412F7D5, 0xA156, 0x4B13, 0x81DC, 0x867174929325)
GUID(guid_gpt_onie_config, 0xD4E6E2CD, 0x4469, 0x46F3, 0xB5CB, 0x1BFF57AFC149)
GUID(guid_gpt_prep, 0x9E1A2D38, 0xC612, 0x4316, 0xAA26, 0x8B49521E5A8B)
GUID(guid_gpt_windows_ldm_data, 0xAF9B60A0, 0x1431, 0x4F62, 0xBC68, 0x3311714A69AD)
GUID(guid_gpt_windows_ldm_metadata, 0x5808C8AA, 0x7E8F, 0x42E0, 0x85D2, 0xE1E90434CFB3)
GUID(guid_gpt_windows_storage_spaces, 0xE75CAF8F, 0xF680, 0x4CEE, 0xAFA3, 0xB001E56EFC2D)
GUID(guid_gpt_ibm_gpfs, 0x37AFFC90, 0xEF7D, 0x4E96, 0x91C3, 0x2D7AE055B174)
GUID(guid_gpt_chromeos_kernel, 0xFE3A2A5D, 0x4F32, 0x41A7, 0xB725, 0xACCC3285A309)
GUID(guid_gpt_chromeos_root, 0x3CB8E202, 0x3B7E, 0x47DD, 0x8A3C, 0x7FF2A13CFCEC)
GUID(guid_gpt_chromeos_reserved, 0x2E0A753D, 0x9E48, 0x43B0, 0x8337, 0xB15192CB1B5E)
GUID(guid_gpt_linux_swap, 0x0657FD6D, 0xA4AB, 0x43C4, 0x84E5, 0x0933C84B4F4F)
GUID(guid_gpt_linux_filesystem, 0x0FC63DAF, 0x8483, 0x4772, 0x8E79, 0x3D69D8477DE4)
GUID(guid_gpt_linux_reserved, 0x8DA63339, 0x0007, 0x60C0, 0xC436, 0x083AC8230908)
GUID(guid_gpt_linux_systemd_home, 0x933AC7E1, 0x2EB4, 0x4F13, 0xB844, 0x0E14E2AEF915)
GUID(guid_gpt_linux_systemd_x86_root, 0x44479540, 0xF297, 0x41B2, 0x9AF7, 0xD131D5F0458A)
GUID(guid_gpt_linux_systemd_x86_64_root, 0x4F68BCE3, 0xE8CD, 0x4DB1, 0x96E7, 0xFBCAF984B709)
GUID(guid_gpt_linux_systemd_arm64_root, 0xB921B045, 0x1DF0, 0x41C3, 0xAF44, 0x4C6F280D3FAE)
GUID(guid_gpt_linux_systemd_srv, 0x3B8F8425, 0x20E0, 0x4F3B, 0x907F, 0x1A25A76F98E8)
GUID(guid_gpt_intel_rapid_start, 0xD3BFE2DE, 0x3DAF, 0x11DF, 0xBA40, 0xE3A556D89593)
GUID(guid_gpt_linux_lvm, 0xE6D6D379, 0xF507, 0x44C2, 0xA23C, 0x238F2A3DF928)
GUID(guid_gpt_freebsd_disklabel, 0x516E7CB4, 0x6ECF, 0x11D6, 0x8FF8, 0x00022D09712B)
GUID(guid_gpt_freebsd_boot, 0x83BD6B9D, 0x7F41, 0x11DC, 0xBE0B, 0x001560B84F0F)
GUID(guid_gpt_freebsd_swap, 0x516E7CB5, 0x6ECF, 0x11D6, 0x8FF8, 0x00022D09712B)
GUID(guid_gpt_freebsd_ufs, 0x516E7CB6, 0x6ECF, 0x11D6, 0x8FF8, 0x00022D09712B)
GUID(guid_gpt_freebsd_zfs, 0x516E7CBA, 0x6ECF, 0x11D6, 0x8FF8, 0x00022D09712B)
GUID(guid_gpt_freebsd_vinum, 0x516E7CB8, 0x6ECF, 0x11D6, 0x8FF8, 0x00022D09712B)
GUID(guid_gpt_midnightbsd_data, 0x85D5E45A, 0x237C, 0x11E1, 0xB4B3, 0xE89A8F7FC3A7)
GUID(guid_gpt_midnightbsd_boot, 0x85D5E45E, 0x237C, 0x11E1, 0xB4B3, 0xE89A8F7FC3A7)
GUID(guid_gpt_midnightbsd_swap, 0x85D5E45B, 0x237C, 0x11E1, 0xB4B3, 0xE89A8F7FC3A7)
GUID(guid_gpt_midnightbsd_ufs, 0x0394EF8B, 0x237E, 0x11E1, 0xB4B3, 0xE89A8F7FC3A7)
GUID(guid_gpt_midnightbsd_zfs, 0x85D5E45D, 0x237C, 0x11E1, 0xB4B3, 0xE89A8F7FC3A7)
GUID(guid_gpt_midnightbsd_vinum, 0x85D5E45C, 0x237C, 0x11E1, 0xB4B3, 0xE89A8F7FC3A7)
GUID(guid_gpt_openbsd_data, 0x824CC7A0, 0x36A8, 0x11E3, 0x890A, 0x952519AD3F61)
GUID(guid_gpt_apple_ufs, 0x55465300, 0x0000, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_freebsd_disklabel_alt, 0x516E7CB4, 0x6ECF, 0x11D6, 0x8FF8, 0x00022D09712B)
GUID(guid_gpt_netbsd_swap, 0x49F48D32, 0xB10E, 0x11DC, 0xB99B, 0x0019D1879648)
GUID(guid_gpt_netbsd_ffs, 0x49F48D5A, 0xB10E, 0x11DC, 0xB99B, 0x0019D1879648)
GUID(guid_gpt_netbsd_lfs, 0x49F48D82, 0xB10E, 0x11DC, 0xB99B, 0x0019D1879648)
GUID(guid_gpt_netbsd_concatenated, 0x2DB519C4, 0xB10F, 0x11DC, 0xB99B, 0x0019D1879648)
GUID(guid_gpt_netbsd_encrypted, 0x2DB519EC, 0xB10F, 0x11DC, 0xB99B, 0x0019D1879648)
GUID(guid_gpt_netbsd_raid, 0x49F48DAA, 0xB10E, 0x11DC, 0xB99B, 0x0019D1879648)
GUID(guid_gpt_apple_boot, 0x426F6F74, 0x0000, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_apple_hfsp, 0x48465300, 0x0000, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_apple_raid, 0x52414944, 0x0000, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_apple_raid_offline, 0x52414944, 0x5F4F, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_apple_label, 0x4C616265, 0x6C00, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_appletv_recovery, 0x5265636F, 0x7665, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_apple_core_storage, 0x53746F72, 0x6167, 0x11AA, 0xAA11, 0x00306543ECAC)
GUID(guid_gpt_solaris_boot, 0x6A82CB45, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_root, 0x6A85CF4D, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_usr, 0x6A898CC3, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_swap, 0x6A87C46F, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_backup, 0x6A8B642B, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_var, 0x6A8EF2E9, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_home, 0x6A90BA39, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_alternate_sector, 0x6A9283A5, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_reserved_1, 0x6A945A3B, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_reserved_2, 0x6A9630D1, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_reserved_3, 0x6A980767, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_reserved_4, 0x6A96237F, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_solaris_reserved_5, 0x6A8D2AC7, 0x1DD2, 0x11B2, 0x99A6, 0x080020736631)
GUID(guid_gpt_hpux_data, 0x75894C1E, 0x3AEB, 0x11D3, 0xB7C1, 0x7B03A0000000)
GUID(guid_gpt_hpux_service, 0xE2A1E728, 0x32E3, 0x11D6, 0xA682, 0x7B03A0000000)
GUID(guid_gpt_freedesktop_boot, 0xBC13C2FF, 0x59E6, 0x4262, 0xA352, 0xB275FD6F7172)
GUID(guid_gpt_haiku_bfs, 0x42465331, 0x3BA3, 0x10F1, 0x802A, 0x4861696B7521)
GUID(guid_gpt_lenovo_esp, 0xBFBFAFE7, 0xA34F, 0x448A, 0x9A5B, 0x6213EB736C22)
GUID(guid_gpt_sony_system_partition, 0xF4019732, 0x066E, 0x4E12, 0x8273, 0x346C5641494F)
GUID(guid_gpt_esp, 0xC12A7328, 0xF81F, 0x11D2, 0xBA4B, 0x00A0C93EC93B)
GUID(guid_gpt_bios_boot, 0x21686148, 0x6449, 0x6E6F, 0x744E, 0x656564454649)
GUID(guid_gpt_mbr_partition_scheme, 0x024DEE41, 0x33E7, 0x11D3, 0x9D69, 0x0008C781F39F)
GUID(guid_gpt_ceph_osd, 0x4FBD7E29, 0x9D25, 0x41B8, 0xAFD0, 0x062C0CEFF05D)
GUID(guid_gpt_ceph_dmcrypt_osd, 0x4FBD7E29, 0x9D25, 0x41B8, 0xAFD0, 0x5EC00CEFF05D)
GUID(guid_gpt_ceph_journal, 0x45B0969E, 0x9B03, 0x4F30, 0xB4C6, 0xB4B80CEFF106)
GUID(guid_gpt_ceph_dmcrypt_journal, 0x45B0969E, 0x9B03, 0x4F30, 0xB4C6, 0x5EC00CEFF106)
GUID(guid_gpt_ceph_disk_in_creation, 0x89C57F98, 0x2FE5, 0x4DC0, 0x89C1, 0xF3AD0CEFF2BE)
GUID(guid_gpt_ceph_dmcrypt_disk_in_creation, 0x89C57F98, 0x2FE5, 0x4DC0, 0x89C1, 0x5EC00CEFF2BE)
GUID(guid_gpt_vmware_vmfs, 0xAA31E02A, 0x400F, 0x11DB, 0x9590, 0x000C2911D1B8)
GUID(guid_gpt_vmware_reserved, 0x9198EFFC, 0x31C0, 0x11DB, 0x8F78, 0x000C2911D1B8)
GUID(guid_gpt_vmware_kcore_crash_protection, 0x9D275380, 0x40AD, 0x11DB, 0xBF97, 0x000C2911D1B8)
GUID(guid_gpt_linux_raid, 0xA19D880F, 0x05FC, 0x4D3B, 0xA006, 0x743F0F84911E)
GUID(guid_gpt_atari_tos, 0x734E5AFE, 0xF61A, 0x11E6, 0xBC64, 0x92361F002671)


#endif //GUID_H
