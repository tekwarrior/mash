/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef GDT_H
#define GDT_H

#define GDT_ACCESS_PRESENT 0x80
#define GDT_ACCESS_PL0 0x00
#define GDT_ACCESS_PL1 0x20
#define GDT_ACCESS_PL2 0x40
#define GDT_ACCESS_PL3 0x60
#define GDT_ACCESS_NOSYS 0x10
#define GDT_ACCESS_EXEC 0x08
#define GDT_ACCESS_DIRECTION 0x04
#define GDT_ACCESS_CONFORMING 0x04
#define GDT_ACCESS_RW 0x02
#define GDT_ACCESS_ACCESSED 0x01

#define GDT_FLAGS_GRANULARITY 0x80
#define GDT_FLAGS_SIZE 0x40

#define GDT_SEL_CODE32 0x0008
#define GDT_SEL_DATA32 0x0010
#define GDT_SEL_CODE16 0x0018
#define GDT_SEL_DATA16 0x0020

#ifndef ASM_SOURCE
#include "tinylib/tinylib.h"

typedef struct PACKED {
    u16 limit0_15;
    u16 base0_15;
    u8 base16_23;
    u8 access;
    u8 flag_limit16_19;
    u8 base24_31;
} gdt_entry_t;

typedef struct PACKED {
    u16 size;
    u32 offset;
} gdt_desc_t;
#endif


#endif //GDT_H
