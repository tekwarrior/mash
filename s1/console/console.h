/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef CONSOLE_H
#define CONSOLE_H

#include "../tinylib/types.h"

enum {
    CON_WIDTH = 80,
    CON_HEIGHT = 25,

    CON_COLOR_BLACK = 0x0,
    CON_COLOR_BLUE = 0x1,
    CON_COLOR_GREEN = 0x2,
    CON_COLOR_CYAN = 0x3,
    CON_COLOR_RED = 0x4,
    CON_COLOR_MAGENTA = 0x5,
    CON_COLOR_BROWN = 0x6,
    CON_COLOR_LGRAY = 0x7,

    CON_COLOR_GRAY = 0x8,
    CON_COLOR_LBLUE = 0x9,
    CON_COLOR_LGREEN = 0xa,
    CON_COLOR_LCYAN = 0xb,
    CON_COLOR_LRED = 0xc,
    CON_COLOR_LMAGENTA = 0xd,
    CON_COLOR_YELLOW = 0xe,
    CON_COLOR_WHITE = 0xf,
};

#define CON_COLOR(fg, bg) ((((bg)&0xf)<<4)|((fg)&0xf))

void con_init(void);

void con_set_color(u8 color);

u8 con_get_color(void);

void con_putc(char c);

void con_puts(char *s);

int con_printf(char *fmt, ...);

void con_clear(void);

void con_scroll(u16 lines);

void con_set_cursor(u8 x, u8 y);

void con_get_cursor(u8 *x, u8 *y);

#endif //CONSOLE_H

