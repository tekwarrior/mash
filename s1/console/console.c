/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdarg.h>

#include "console.h"
#include "../tinylib/tinylib.h"
#include "../bios/bios.h"

static u8 con_x, con_y;
static u8 con_color;
static volatile u16 *con_buffer = (u16*)0xb8000;

void con_init() {
    con_color = CON_COLOR(CON_COLOR_LGRAY, CON_COLOR_BLACK);
    con_clear();
}

void con_set_color(u8 color) {
    con_color = color;
}

u8 con_get_color() {
    return con_color;
}

void con_putc(char c) {
    index offset = (con_y * CON_WIDTH) + con_x;
    u16 value = (con_color << 8) | (u8)c;

    switch (c) {
        case '\r':
            con_x = 0;
            break;
        case '\v':
            con_y++;
            break;
        case '\n':
            con_y++;
            con_x = 0;
            break;
        case '\t':
            con_x += 8 - (con_x & 7);
            break;
        default:
            con_buffer[offset] = value;
            con_x++;
            break;
    }

    if (con_x >= CON_WIDTH) {
        con_x -= CON_WIDTH;
        con_y++;
    }

    if (con_y == CON_HEIGHT) {
        con_scroll(1);
    }

    int10_update_cursor(con_x, con_y);
}

void con_puts(char *s) {
    while (*s) con_putc(*s++);
}

static void con_emitc(void *data, char c) {
    con_putc(c);
}

int con_printf(char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    int result = fncprintf(con_emitc, 0, fmt, args);
    va_end(args);
    return result;
}

void con_clear(void) {
    for (int i = 0; i < CON_WIDTH * CON_HEIGHT; i++) {
        con_buffer[i] = (con_color << 8) | 0x20;
    }
    con_set_cursor(0, 0);
}

void con_scroll(u16 lines) {
    if (lines > CON_HEIGHT) lines = CON_HEIGHT;

    int offset = lines * CON_WIDTH;
    int move_count = CON_WIDTH * CON_HEIGHT - offset;

    for (int i = 0; i < CON_WIDTH * CON_HEIGHT; i++) {
        if (i < move_count) con_buffer[i] = con_buffer[i+offset];
        else con_buffer[i] = (con_color << 8) | 0x20;
    }

    // Adjust coordinate
    if (lines > con_y) con_y = 0;
    else con_y -= (u8)lines;

    int10_update_cursor(con_x, con_y);
}

void con_set_cursor(u8 x, u8 y) {
    con_x = x;
    con_y = y;

    int10_update_cursor(con_x, con_y);
}

void con_get_cursor(u8 *x, u8 *y) {
    *x = con_x;
    *y = con_y;
}

