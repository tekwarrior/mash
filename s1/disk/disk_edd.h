/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef DISK_EDD_H
#define DISK_EDD_H

#include "../tinylib/tinylib.h"
#include "disk.h"

enum {
    EDD_MAX_DISKS = 127
};

typedef struct PACKED {
        u16 size;
        u16 info_flags;
        u32 cyl;
        u32 heads;
        u32 sec_per_track;
        u64 sectors;
        u16 bytes_per_sector;
        // v2.0
        u32 config;
        // v3.0
        u16 dp_sig;
        u8 dp_len;
        u8 reserved1[3];
        char bus[4];
        char interface[8];
        u8 int_path[8];
        u8 dev_path[8];
        u8 reserved2;
        u8 chksum;
} disk_edd_drive_parameters_t;

typedef struct {
    disk_driver_interface_t interface;
    u8 drive_count;
    u8 drive_numbers[EDD_MAX_DISKS];
    u8 edd_versions[EDD_MAX_DISKS];
} disk_edd_driver_interface_t;

typedef struct {
    disk_interface_t interface;
    u8 drive_number;
    u8 edd_version;
    disk_info_t info;
    disk_edd_drive_parameters_t parameters;
} disk_edd_disk_interface_t;


void disk_edd_init();

int disk_edd_scan(disk_driver_interface_t *disk_driver_data);

void *disk_edd_open(disk_driver_interface_t *disk_driver_data, int disk_number);

void disk_edd_close(disk_interface_t *disk_data);

disk_info_t const *disk_edd_get_info(disk_interface_t *disk_data);

int disk_edd_read(disk_interface_t *disk_data, u64 lba, int count, void *buffer);

int disk_edd_write(disk_interface_t *disk_data, u64 lba, int count, void *buffer);

#endif //DISK_EDD_H
