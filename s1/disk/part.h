/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef PART_H
#define PART_H

#include "../tinylib/tinylib.h"
#include "disk.h"

typedef struct part_driver_interface_t part_driver_interface_t;
typedef struct part_interface_t part_interface_t;
typedef struct part_info_t part_info_t;

struct part_driver_interface_t {
    char name[8];
    int (*scan)(part_driver_interface_t *, disk_interface_t*);
    part_interface_t *(*open)(part_driver_interface_t *, disk_interface_t*, int partnum);
};

typedef struct part_interface_t {
    void (*close)(part_interface_t *part);
    const part_info_t *(*get_info)(part_interface_t *part);
    int (*read)(part_interface_t *part, u64 start_block, int count, void *buffer);
    int (*write)(part_interface_t *part, u64 start_block, int count, void *buffer);
} part_interface_t;

struct part_info_t {
    u64 start_block;
    u64 block_count;
    u32 block_size;
};

void part_init();
void part_register_driver(part_driver_interface_t *driver);
part_driver_interface_t *part_get_driver(const char *name);

static inline int part_scan(part_driver_interface_t *driver, disk_interface_t *disk) {
    return driver->scan(driver, disk);
}

static inline part_interface_t *part_open(part_driver_interface_t *driver, disk_interface_t *disk, int partnum) {
    return driver->open(driver, disk, partnum);
}

static inline void part_close(part_interface_t *part) {
    part->close(part);
}

static inline const part_info_t *part_get_info(part_interface_t *part) {
    return part->get_info(part);
}

static inline int part_read(part_interface_t *part, u64 start_block, int count, void *buffer) {
    return part->read(part, start_block, count, buffer);
}

static inline int part_write(part_interface_t *part, u64 start_block, int count, void *buffer) {
    return part->write(part, start_block, count, buffer);
}

#endif //PART_H
