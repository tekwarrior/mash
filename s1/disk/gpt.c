/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "gpt.h"

typedef struct gpt_type_guid_map_t {
    guid_t *g;
    char *name;
} gpt_type_guid_map_t;

static gpt_type_guid_map_t gpt_type_names[] = {
        {&guid_empty,                             "empty"},
        {&guid_gpt_microsoft_basic,               "microsoft_basic"},
        {&guid_gpt_microsoft_reserved,            "microsoft_reserved"},
        {&guid_gpt_microsoft_re,                  "microsoft_re"},
        {&guid_gpt_onie_boot,                     "onie_boot"},
        {&guid_gpt_onie_config,                   "onie_config"},
        {&guid_gpt_prep,                          "prep"},
        {&guid_gpt_windows_ldm_data,              "windows_ldm_data"},
        {&guid_gpt_windows_ldm_metadata,          "windows_ldm_metadata"},
        {&guid_gpt_windows_storage_spaces,        "windows_storage_spaces"},
        {&guid_gpt_ibm_gpfs,                      "ibm_gpfs"},
        {&guid_gpt_chromeos_kernel,               "chromeos_kernel"},
        {&guid_gpt_chromeos_root,                 "chromeos_root"},
        {&guid_gpt_chromeos_reserved,             "chromeos_reserved"},
        {&guid_gpt_linux_swap,                    "linux_swap"},
        {&guid_gpt_linux_filesystem,              "linux_filesystem"},
        {&guid_gpt_linux_reserved,                "linux_reserved"},
        {&guid_gpt_linux_systemd_home,            "linux_systemd_home"},
        {&guid_gpt_linux_systemd_x86_root,        "linux_systemd_x86_root"},
        {&guid_gpt_linux_systemd_x86_64_root,     "linux_systemd_x86_64_root"},
        {&guid_gpt_linux_systemd_arm64_root,      "linux_systemd_arm64_root"},
        {&guid_gpt_linux_systemd_srv,             "linux_systemd_srv"},
        {&guid_gpt_intel_rapid_start,             "intel_rapid_start"},
        {&guid_gpt_linux_lvm,                     "linux_lvm"},
        {&guid_gpt_freebsd_disklabel,             "freebsd_disklabel"},
        {&guid_gpt_freebsd_boot,                  "freebsd_boot"},
        {&guid_gpt_freebsd_swap,                  "freebsd_swap"},
        {&guid_gpt_freebsd_ufs,                   "freebsd_ufs"},
        {&guid_gpt_freebsd_zfs,                   "freebsd_zfs"},
        {&guid_gpt_freebsd_vinum,                 "freebsd_vinum"},
        {&guid_gpt_midnightbsd_data,              "midnightbsd_data"},
        {&guid_gpt_midnightbsd_boot,              "midnightbsd_boot"},
        {&guid_gpt_midnightbsd_swap,              "midnightbsd_swap"},
        {&guid_gpt_midnightbsd_ufs,               "midnightbsd_ufs"},
        {&guid_gpt_midnightbsd_zfs,               "midnightbsd_zfs"},
        {&guid_gpt_midnightbsd_vinum,             "midnightbsd_vinum"},
        {&guid_gpt_openbsd_data,                  "openbsd_data"},
        {&guid_gpt_apple_ufs,                     "apple_ufs"},
        {&guid_gpt_freebsd_disklabel_alt,         "freebsd_disklabel_alt"},
        {&guid_gpt_netbsd_swap,                   "netbsd_swap"},
        {&guid_gpt_netbsd_ffs,                    "netbsd_ffs"},
        {&guid_gpt_netbsd_lfs,                    "netbsd_lfs"},
        {&guid_gpt_netbsd_concatenated,           "netbsd_concatenated"},
        {&guid_gpt_netbsd_encrypted,              "netbsd_encrypted"},
        {&guid_gpt_netbsd_raid,                   "netbsd_raid"},
        {&guid_gpt_apple_boot,                    "apple_boot"},
        {&guid_gpt_apple_hfsp,                    "apple_hfsp"},
        {&guid_gpt_apple_raid,                    "apple_raid"},
        {&guid_gpt_apple_raid_offline,            "apple_raid_offline"},
        {&guid_gpt_apple_label,                   "apple_label"},
        {&guid_gpt_appletv_recovery,              "appletv_recovery"},
        {&guid_gpt_apple_core_storage,            "apple_core_storage"},
        {&guid_gpt_solaris_boot,                  "solaris_boot"},
        {&guid_gpt_solaris_root,                  "solaris_root"},
        {&guid_gpt_solaris_usr,                   "solaris_usr"},
        {&guid_gpt_solaris_swap,                  "solaris_swap"},
        {&guid_gpt_solaris_backup,                "solaris_backup"},
        {&guid_gpt_solaris_var,                   "solaris_var"},
        {&guid_gpt_solaris_home,                  "solaris_home"},
        {&guid_gpt_solaris_alternate_sector,      "solaris_alternate_sector"},
        {&guid_gpt_solaris_reserved_1,            "solaris_reserved_1"},
        {&guid_gpt_solaris_reserved_2,            "solaris_reserved_2"},
        {&guid_gpt_solaris_reserved_3,            "solaris_reserved_3"},
        {&guid_gpt_solaris_reserved_4,            "solaris_reserved_4"},
        {&guid_gpt_solaris_reserved_5,            "solaris_reserved_5"},
        {&guid_gpt_hpux_data,                     "hpux_data"},
        {&guid_gpt_hpux_service,                  "hpux_service"},
        {&guid_gpt_freedesktop_boot,              "freedesktop_boot"},
        {&guid_gpt_haiku_bfs,                     "haiku_bfs"},
        {&guid_gpt_lenovo_esp,                    "lenovo_esp"},
        {&guid_gpt_sony_system_partition,         "sony_system_partition"},
        {&guid_gpt_esp,                           "esp"},
        {&guid_gpt_bios_boot,                     "bios_boot"},
        {&guid_gpt_mbr_partition_scheme,          "mbr_partition_scheme"},
        {&guid_gpt_ceph_osd,                      "ceph_osd"},
        {&guid_gpt_ceph_dmcrypt_osd,              "ceph_dmcrypt_osd"},
        {&guid_gpt_ceph_journal,                  "ceph_journal"},
        {&guid_gpt_ceph_dmcrypt_journal,          "ceph_dmcrypt_journal"},
        {&guid_gpt_ceph_disk_in_creation,         "ceph_disk_in_creation"},
        {&guid_gpt_ceph_dmcrypt_disk_in_creation, "ceph_dmcrypt_disk_in_creation"},
        {&guid_gpt_vmware_vmfs,                   "vmware_vmfs"},
        {&guid_gpt_vmware_reserved,               "vmware_reserved"},
        {&guid_gpt_vmware_kcore_crash_protection, "vmware_kcore_crash_protection"},
        {&guid_gpt_linux_raid,                    "linux_raid"},
        {&guid_gpt_atari_tos,                     "atari_tos"},
};

const char *gpt_part_type_name(guid_t *type_guid) {
    unsigned type_name_count = sizeof gpt_type_names / sizeof gpt_type_names[0];

    for (unsigned i = 0; i < type_name_count; i++) {
        if (guid_cmp(type_guid, gpt_type_names[i].g) == 0) return gpt_type_names[i].name;
    }

    return 0;
}

static gpt_driver_interface_t gpt_driver = {
        .interface.name = "gpt",
        .interface.open = gpt_open,
        .interface.scan = gpt_scan
};

STATIC_INIT(gpt_init)

void gpt_init() {
    part_register_driver(&gpt_driver.interface);
}

static int gpt_check_header(gpt_header_t *header) {
    if (header->signature != GPT_HEADER_SIGNATURE) return 0;
    return 1;
}

static void *gpt_alloc_and_read_sector(disk_interface_t *disk, u64 sector) {
    const disk_info_t *disk_info = disk_get_info(disk);
    if (sector >= disk_info->block_count) return 0;

    void *buffer = mem_alloc(disk_info->block_size);
    if (!buffer) return 0;

    if (disk_read(disk, sector, 1, buffer) != 1) {
        mem_free(buffer);
        return 0;
    }

    return buffer;
}

static gpt_header_t *gpt_alloc_and_read_header(disk_interface_t *disk) {
    gpt_header_t *header_buffer = gpt_alloc_and_read_sector(disk, 1);
    if (!header_buffer) return 0;
    return header_buffer;
}

int gpt_scan(part_driver_interface_t *driver, disk_interface_t *disk) {
    gpt_header_t *header_buffer = gpt_alloc_and_read_header(disk);
    if (!header_buffer) abort("unable to allocate sector buffer to read gpt header");
    if (!gpt_check_header(header_buffer)) {
        // Not a GPT partitioned disk
        mem_free(header_buffer);
        return 0;
    }

    // TODO get real number of partitions that actually exist and map the index in gpt_open
    int part_count = (int) header_buffer->partition_count;
    mem_free(header_buffer);
    return part_count;
}

part_interface_t *gpt_open(part_driver_interface_t *driver, disk_interface_t *disk, int partnum) {
    const disk_info_t *disk_info = disk_get_info(disk);

    gpt_header_t *header_buffer = gpt_alloc_and_read_header(disk);
    if (!header_buffer) return 0;
    if (partnum >= header_buffer->partition_count) {
        mem_free(header_buffer);
        return 0;
    }

    // Determine partition table entry sector and offset
    u64 entry_offset = partnum * header_buffer->entry_size;
    u64 entry_sector = entry_offset / disk_info->block_size + 2;
    u32 entry_sector_offset = entry_offset % disk_info->block_size;

    // Discard header as it is not needed anymore
    mem_free(header_buffer);

    gpt_entry_t *table_slice = gpt_alloc_and_read_sector(disk, entry_sector);
    if (!table_slice) {
        return 0;
    }

    // Allocate and initialize partition data and copy entry
    gpt_part_interface_t *part = mem_alloc(sizeof *part);
    if (!part) {
        mem_free(table_slice);
        return 0;
    }
    mem_clr(part, sizeof *part);
    mem_copy((u8*)table_slice + entry_sector_offset, &part->entry, sizeof part->entry);

    part->disk = disk;
    part->interface.close = gpt_close;
    part->interface.get_info = gpt_get_info;
    part->interface.write = gpt_write;
    part->interface.read = gpt_read;
    part->info.block_size = disk_info->block_size;
    part->info.start_block = part->entry.first_lba;
    part->info.block_count = part->entry.last_lba - part->entry.first_lba;

    mem_free(table_slice);

    return &part->interface;
}

void gpt_close(part_interface_t *part) {
    gpt_part_interface_t *interface = (gpt_part_interface_t *) part;
    mem_free(interface);
}

const part_info_t *gpt_get_info(part_interface_t *part) {
    gpt_part_interface_t *interface = (gpt_part_interface_t *) part;
    return &interface->info;
}

int gpt_read(part_interface_t *part, u64 start_block, int count, void *buffer) {
    gpt_part_interface_t *interface = (gpt_part_interface_t *) part;

    // Limit read to inside the partition
    u64 max_count = interface->info.block_count - start_block;
    if (count > max_count) count = (int) max_count;
    if (count < 0) return E_ARG;

    return disk_read(interface->disk, interface->info.start_block + start_block, count, buffer);
}

int gpt_write(part_interface_t *part, u64 start_block, int count, void *buffer) {
    gpt_part_interface_t *interface = (gpt_part_interface_t *) part;

    // Limit write to inside the partition
    u64 max_count = interface->info.block_count - start_block;
    if (count > max_count) count = (int) max_count;
    if (count < 0) return E_ARG;

    return disk_write(interface->disk, interface->info.start_block + start_block, count, buffer);
}
