/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "../console/console.h"

#include "disk_edd.h"
#include "disk.h"
#include "../tinylib/tinylib.h"
#include "../bios/bios.h"

STATIC_INIT(disk_edd_init)

static inline u8 disk_edd_drive_number(u8 index) {
    return (u8) 0x80 + index;
}

// Driver function table
static disk_edd_driver_interface_t disk_edd_driver_interface = {
        .interface.name = "disk",
        .interface.open = disk_edd_open,
        .interface.scan = disk_edd_scan,
        .drive_count = 0,
        .drive_numbers = {0},
        .edd_versions = {0}
};

void disk_edd_init() {
    disk_register_driver(&disk_edd_driver_interface.interface);
}

int disk_edd_scan(disk_driver_interface_t *disk_driver_data) {
    disk_edd_driver_interface_t *driver_interface = (disk_edd_driver_interface_t *) disk_driver_data;

    u8 max_disks = *(u8 *) rmaddr_to_ptr(0x0040, 0x0075);
    if (max_disks > EDD_MAX_DISKS) return -1;

    for (u8 i = 0; i < max_disks; i++) {
        u8 drive = disk_edd_drive_number(i);
        u8 edd_version = int13_edd_install_check(drive);
        if (edd_version != 0) {
            driver_interface->drive_numbers[driver_interface->drive_count] = drive;
            driver_interface->edd_versions[driver_interface->drive_count] = edd_version;
            driver_interface->drive_count++;
        }
    }

    return driver_interface->drive_count;
}

void *disk_edd_open(disk_driver_interface_t *disk_driver_data, int disk_number) {
    disk_edd_driver_interface_t *driver_interface = (disk_edd_driver_interface_t *) disk_driver_data;
    if (disk_number >= driver_interface->drive_count) return 0;

    disk_edd_disk_interface_t *disk = mem_alloc(sizeof *disk);
    if (!disk) return 0;

    disk->interface.close = disk_edd_close;
    disk->interface.info = disk_edd_get_info;
    disk->interface.read = disk_edd_read;
    disk->interface.write = disk_edd_write;
    disk->drive_number = driver_interface->drive_numbers[disk_number];
    disk->edd_version = driver_interface->edd_versions[disk_number];

    mem_clr(&disk->parameters, sizeof disk->parameters);
    disk->parameters.size = sizeof disk->parameters;

    // Retrieve geometry
    u8 param_result = int13_edd_get_disk_parameters(&disk->parameters, disk->drive_number);
    if (param_result != 0) {
        mem_free(disk);
        return 0;
    }
    disk->info.block_count = disk->parameters.sectors;
    disk->info.block_size = disk->parameters.bytes_per_sector;

    return &disk->interface;
}

void disk_edd_close(disk_interface_t *disk_data) {
    mem_free(disk_data);
}

disk_info_t const *disk_edd_get_info(disk_interface_t *disk_data) {
    disk_edd_disk_interface_t *interface = (disk_edd_disk_interface_t *) disk_data;
    return &(interface->info);
}

int disk_edd_read(disk_interface_t *disk_data, u64 lba, int count, void *buffer) {
    if (count < 0) return E_ARG;

    disk_edd_disk_interface_t *interface = (disk_edd_disk_interface_t *) disk_data;

    u16 buffer_seg, buffer_off;
    ptr_to_rmaddr(buffer, &buffer_seg, &buffer_off);
    static disk_edd_address_packet_t address_packet;

    address_packet.size = sizeof address_packet,
            address_packet.reserved = 0;
    address_packet.sector = lba;
    address_packet.target_off = buffer_off;
    address_packet.target_seg = buffer_seg;
    address_packet.sector_count = (u16) count;
    address_packet.target_addr = (u64) buffer;

    u8 result = int13_edd_extended_read(&address_packet, interface->drive_number);
    if (result != 0) {
        return 0;
    }

    return address_packet.sector_count;
}

int disk_edd_write(disk_interface_t *disk_data, u64 lba, int count, void *buffer) {
    return 0;
}
