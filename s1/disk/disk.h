/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef DISK_H
#define DISK_H

#include "../tinylib/types.h"

typedef struct disk_interface_t disk_interface_t;
typedef struct disk_driver_interface_t disk_driver_interface_t;
typedef struct disk_info_t disk_info_t;

typedef int (*disk_driver_fn_scan)(disk_driver_interface_t *disk_driver_data);
typedef void *(*disk_driver_fn_open)(disk_driver_interface_t *disk_driver_data, int disk_number);

typedef void (*disk_fn_close)(disk_interface_t *disk_data);
typedef disk_info_t const *(*disk_fn_get_info)(disk_interface_t *disk_data);
typedef int (*disk_fn_read)(disk_interface_t *disk_data, u64 lba, int count, void *buffer);
typedef int (*disk_fn_write)(disk_interface_t *disk_data, u64 lba, int count, void *buffer);

struct disk_info_t {
    u32 block_size;
    u64 block_count;
};

// When registering a driver use a specialized variant which has its data appended as needed
struct disk_driver_interface_t {
    char name[8];
    disk_driver_fn_scan scan;
    disk_driver_fn_open open;
};

struct disk_interface_t {
    disk_fn_close close;
    disk_fn_get_info info;
    disk_fn_read read;
    disk_fn_write write;
};

void disk_init();
void disk_register_driver(disk_driver_interface_t *interface);
disk_driver_interface_t *disk_get_driver(const char *name);

// Convenience inline functions that forward to the struct only
static inline int disk_scan(disk_driver_interface_t *disk_driver_data) {
    return disk_driver_data->scan(disk_driver_data);
}

static inline void *disk_open(disk_driver_interface_t *disk_driver_data, int disk_number) {
    return disk_driver_data->open(disk_driver_data, disk_number);
}

static inline void disk_close(disk_interface_t *disk_data) {
    disk_data->close(disk_data);
}

static inline disk_info_t const *disk_get_info(disk_interface_t *disk_data) {
    return disk_data->info(disk_data);
}

static inline int disk_read(disk_interface_t *disk_data, u64 lba, int count, void *buffer) {
    return disk_data->read(disk_data, lba, count, buffer);
}

static inline int disk_write(disk_interface_t *disk_data, u64 lba, int count, void *buffer) {
    return disk_data->write(disk_data, lba, count, buffer);
}

#endif //DISK_H
