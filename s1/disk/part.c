/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "part.h"

typedef struct part_driver_list_entry_t {
    struct part_driver_list_entry_t *next;
    part_driver_interface_t *interface;
} part_driver_list_entry_t;

static part_driver_list_entry_t *part_driver_list;
static part_driver_list_entry_t *part_driver_list_end;

void part_init() {
    part_driver_list = 0;
    part_driver_list_end = 0;
}

void part_register_driver(part_driver_interface_t *driver) {
    part_driver_list_entry_t *entry = mem_alloc(sizeof *entry);
    if (!entry) abort ("unable to register part driver - alloc failed");

    entry->next = 0;
    entry->interface = driver;

    if (!part_driver_list) {
        part_driver_list = entry;
        part_driver_list_end = entry;
    } else {
        part_driver_list_end->next = entry;
        part_driver_list_end = entry;
    }
}

part_driver_interface_t *part_get_driver(const char *name) {
    part_driver_list_entry_t *entry = part_driver_list;

    while (entry) {
        if (str_cmp_n(entry->interface->name, name, sizeof entry->interface->name) == 0) return entry->interface;
        entry = entry->next;
    }

    return 0;
}
