/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef GPT_H
#define GPT_H

#include "../tinylib/tinylib.h"
#include "disk.h"
#include "part.h"

typedef struct PACKED gpt_header_t {
    u64 signature;
    u32 revision;
    u32 header_size;
    u32 header_crc32;
    u32 reserved;
    u64 header_lba;
    u64 backup_header_lba;
    u64 first_usable_lba;
    u64 last_usable_lba;
    guid_t disk_guid;
    u64 table_lba;
    u32 partition_count;
    u32 entry_size;
    u32 table_crc32;
} gpt_header_t;

typedef struct PACKED gpt_entry_t {
    guid_t type_guid;
    guid_t entry_guid;
    u64 first_lba;
    u64 last_lba;
    u64 attr;
    u16 name[36];
} gpt_entry_t;

const char *gpt_part_type_name(guid_t *type_guid);

#define GPT_HEADER_SIGNATURE (0x5452415020494645ull)

typedef struct {
    part_driver_interface_t interface;
} gpt_driver_interface_t;

typedef struct {
    part_interface_t interface;
    part_info_t info;
    disk_interface_t *disk;
    gpt_entry_t entry;
} gpt_part_interface_t;

void gpt_init();
int gpt_scan(part_driver_interface_t *driver, disk_interface_t *disk);
part_interface_t *gpt_open(part_driver_interface_t *driver, disk_interface_t *disk, int partnum);
void gpt_close(part_interface_t *part);
const part_info_t *gpt_get_info(part_interface_t *part);
int gpt_read(part_interface_t *part, u64 start_block, int count, void *buffer);
int gpt_write(part_interface_t *part, u64 start_block, int count, void *buffer);

#endif //GPT_H
