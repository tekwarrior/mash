/*
 * Copyright (c) 2018, Marc Thrun
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 	* Redistributions of source code must retain the above copyright
 * 	  notice, this list of conditions and the following disclaimer.
 * 	* Redistributions in binary form must reproduce the above copyright
 * 	  notice, this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 * 	* Neither the name of the <organization> nor the
 * 	  names of its contributors may be used to endorse or promote products
 * 	  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "idt.h"
#include "gdt.h"

static inline void idt_setup(u16 index, u16 sel, void (*off)(), u8 type, u8 dpl, u8 present, u8 storage) {
    idt[index].off_lo = (u32)off & 0xffff;
    idt[index].sel = sel;
    idt[index].flags = type | dpl | present | storage;
    idt[index].off_hi = (u32)off >> 16;
}

// Declare all irq functions
#define IDT_IRQ_FN(hi, lo) extern void irq_ ## hi ## lo ## _entry();
#define IDT_IRQ_FN_BLOCK(hi) \
IDT_IRQ_FN(hi, 0) IDT_IRQ_FN(hi, 1) IDT_IRQ_FN(hi, 2) IDT_IRQ_FN(hi, 3) \
IDT_IRQ_FN(hi, 4) IDT_IRQ_FN(hi, 5) IDT_IRQ_FN(hi, 6) IDT_IRQ_FN(hi, 7) \
IDT_IRQ_FN(hi, 8) IDT_IRQ_FN(hi, 9) IDT_IRQ_FN(hi, a) IDT_IRQ_FN(hi, b) \
IDT_IRQ_FN(hi, c) IDT_IRQ_FN(hi, d) IDT_IRQ_FN(hi, e) IDT_IRQ_FN(hi, f)

IDT_IRQ_FN_BLOCK(0) IDT_IRQ_FN_BLOCK(1) IDT_IRQ_FN_BLOCK(2) IDT_IRQ_FN_BLOCK(3) \
IDT_IRQ_FN_BLOCK(4) IDT_IRQ_FN_BLOCK(5) IDT_IRQ_FN_BLOCK(6) IDT_IRQ_FN_BLOCK(7) \
IDT_IRQ_FN_BLOCK(8) IDT_IRQ_FN_BLOCK(9) IDT_IRQ_FN_BLOCK(a) IDT_IRQ_FN_BLOCK(b) \
IDT_IRQ_FN_BLOCK(c) IDT_IRQ_FN_BLOCK(d) IDT_IRQ_FN_BLOCK(e) IDT_IRQ_FN_BLOCK(f)

#define IDT_INIT_BLOCK(index, prefix) \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 0_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 1_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 2_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 3_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 4_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 5_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 6_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 7_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 8_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## 9_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## a_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## b_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## c_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## d_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## e_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0); \
idt_setup(index++, GDT_SEL_CODE32, irq_ ## prefix ## f_entry, IDT_TYPE_INT32, IDT_DPL0, IDT_PRESENT, 0);

idt_entry_t idt[256];

void idt_init() {
    u16 i = 0;
    IDT_INIT_BLOCK(i, 0)
    IDT_INIT_BLOCK(i, 1)
    IDT_INIT_BLOCK(i, 2)
    IDT_INIT_BLOCK(i, 3)
    IDT_INIT_BLOCK(i, 4)
    IDT_INIT_BLOCK(i, 5)
    IDT_INIT_BLOCK(i, 6)
    IDT_INIT_BLOCK(i, 7)
    IDT_INIT_BLOCK(i, 8)
    IDT_INIT_BLOCK(i, 9)
    IDT_INIT_BLOCK(i, a)
    IDT_INIT_BLOCK(i, b)
    IDT_INIT_BLOCK(i, c)
    IDT_INIT_BLOCK(i, d)
    IDT_INIT_BLOCK(i, e)
    IDT_INIT_BLOCK(i, f)
}