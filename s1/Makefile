export CFLAGS = -ffreestanding -flto -Os -fno-exceptions -m32
export CFLAGS16 = $(CFLAGS) -m16
export ASFLAGS = -ffreestanding -Os -m32
export LLVMLINKFLAGS =
export LLCFLAGS = -filetype=obj -march=x86 -mcpu=i486

LDFLAGS = -T link.ld -m elf_i386 --lto-O3 --oformat binary --Map link.map

SUBDIRS = tinylib console disk bios

BIN = s1.bin
ASM_OBJ = entry.o mode.o irq.o
COMPILED_BITCODE = s1_compiled.o
LINKED_BITCODE = s1_linked.bc
BITCODE = main.bc init.bc idt.bc interrupt.bc
BITCODE += $(patsubst %,%.bc,$(SUBDIRS))

SUBDIRS_CLEAN = $(patsubst %,clean-%,$(SUBDIRS))

.SUFFIXES:
.PHONY: all clean

all: $(BIN)

clean: $(SUBDIRS_CLEAN)
	rm -f $(BIN) $(OBJ) $(LINKED_BITCODE) $(BITCODE) $(ASM_OBJ)

$(BIN): $(ASM_OBJ) $(COMPILED_BITCODE)
	ld.lld $(LDFLAGS) -o $(BIN) $(ASM_OBJ) $(COMPILED_BITCODE)

$(COMPILED_BITCODE): $(LINKED_BITCODE)
	llc $(LLCFLAGS) -o $(COMPILED_BITCODE) $(LINKED_BITCODE)

$(LINKED_BITCODE): $(BITCODE)
	llvm-link -o $(LINKED_BITCODE) $(BITCODE)

%.bc : %.c ; clang $(CFLAGS) -c -o $@ $<
%.o : %.S ; clang $(ASFLAGS) -c -o $@ $<

# Subdirectories
%.bc : % ; make -C $< all
clean-% : % ; make -C $< clean

