//
// Created by tkw on 22/11/18.
//

#include "tinylib/tinylib.h"
#include "console/console.h"
#include "disk/disk.h"
#include "disk/part.h"
#include "idt.h"

extern void main(void);
extern u8 bss_begin[];
extern u8 bss_end[];
extern void (*init_begin[])();
extern void (*init_end[])();

void init() {
    mem_clr(bss_begin, bss_end - bss_begin);
    idt_init();
    tinylib_init();
    con_init();
    disk_init();
    part_init();

    for (index i = 0; i < init_end - init_begin; i++) {
        void (*init_fn)(void) = *init_begin[i];
        init_fn();
    }
}
