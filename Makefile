SUBDIRS = s0 s1

SUBDIRS_BUILD = $(patsubst %,build-%,$(SUBDIRS))
SUBDIRS_CLEAN = $(patsubst %,clean-%,$(SUBDIRS))
SUBDIRS_COLLECT = $(patsubst %,collect-%,$(SUBDIRS))

DIST_DIR = dist

.PHONY: all build clean collect collect-prepare collect-clean $(SUBDIRS) $(SUBDIRS_CLEAN) $(SUBDIRS_COLLECT) $(SUBDIRS_BUILD)

all: build collect
build: $(SUBDIRS_BUILD)
clean: $(SUBDIRS_CLEAN) collect-clean
collect: collect-prepare $(SUBDIRS_COLLECT)
collect-prepare: $(DIST_DIR)
collect-clean: ; rm -rf $(DIST_DIR)

$(DIST_DIR): ; mkdir -p $(DIST_DIR)

build-s0: ; make -C s0
build-s1: ; make -C s1

clean-s0: ; make -C s0 clean
clean-s1: ; make -C s1 clean

collect-s0: ; cp s0/s0.bin $(DIST_DIR)/
collect-s1: ; cp s1/s1.bin $(DIST_DIR)/
